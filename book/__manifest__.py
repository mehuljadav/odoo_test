{
    'name': 'goodreads',
    'description': 'Get a ride in minutes. Or become a driver and earn money on your schedule. '
                   'Uber is finding you better ways to move, work, and succeed.',
    'author': 'goodreads, Inc. ',
    'website': 'www.goodreads.com',
    'version': "11.0.1.0.0",
    'depends': ['base'],
    'data': [
        'views/book_view.xml',
        'views/author_view.xml',
        'views/user_view.xml',
        'views/audio_view.xml',
        'views/payment_view.xml',
        'views/event_view.xml'
    ]

}