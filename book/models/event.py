from odoo import fields,models

class Event(models.Model):
    _name = 'event.event'

    name = fields.Char('Name')
    start_date = fields.Datetime('Start', required=True,default=fields.Date.today)
    end_date = fields.Datetime('End', required=True)
    amount = fields.Integer('Amount')
    select = fields.Selection([('one', 'One'),
                                  ('two', 'Two'),
                                  ('three', 'Three')], 'Type', required=True)
    description = fields.Text('Description')

    # user_ids = fields.One2many('user.user','event_id','Users')
    user_ids = fields.Many2many('user.user','event_user_rel','event_id','user_id',string='User')
    # event_id = fields.Many2one('event.event', 'Event')