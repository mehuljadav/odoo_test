from odoo import models,fields

class Audio(models.Model):
    _name = 'audio.audio'

    name = fields.Char('Name')
    writtenby = fields.Char('Writtenby')
    length = fields.Char('Length')
    email = fields.Char('Email')
    price = fields.Float('Price')

    user_ids = fields.Many2many('user.user', 'audio_user_rel', 'audio_id', 'user_id', String='User')
    payment_id = fields.Many2one('payment.payment','Payment')