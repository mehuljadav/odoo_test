from odoo import models,fields

class Payment(models.Model):
    _name = 'payment.payment'

    bankname = fields.Char('Bank')
    payment = fields.Selection([('credit','Credit Card'),('debit','Debit Card'),('upi','UPI')],'Payment')
    status = fields.Char('Status',default="-",readonly=True)
    aadhar = fields.Char('Aadhar')
    # time = fields.Datetime.now('time')

    user_id = fields.Many2one('user.user','User')
