from odoo import models,fields,api

class Book(models.Model):
    _name = 'book.book'

    name = fields.Char('Name')
    description = fields.Text('Description')
    pub_year = fields.Date('Pub_year')
    price = fields.Integer('Price',required=True)

    author_ids = fields.One2many('author.author','book_id','author')
    user_ids = fields.Many2many('user.user','book_user_rel','book_id','user_id',String='User')
    option = fields.Selection([('wait','Wait'),
                               ('start','Start'),
                               ('end','End'),
                               ('cancle','Cancle')],'Option',default='wait')

    @api.multi
    def detail(self):
        print("\n User Detail... \n")
        return {
            'name': 'Details',
            'view_type': 'form',
            'view_mode': 'tree,form',
            'res_model': 'user.user',
            'domain': [('id', 'in', self.user_ids.ids)],
            'type': 'ir.actions.act_window',
            #            'target': 'new',
        }

    @api.multi
    def detailone(self):
        print("\n Author Detail... \n")
        return {
            'name': 'Detailone',
            'view_type': 'form',
            'view_mode': 'tree,form',
            'res_model': 'author.author',
            'domain': [('id', 'in', self.author_ids.ids)],
            'type': 'ir.actions.act_window',
            #            'target': 'new',
        }

    @api.multi
    def start(self):
        record = self.read(['name','description','authors_ids','user_ids'])
        self.write({'option':'start'})
        print("\n Book Available..!!!!")

    @api.multi
    def end(self):
        self.write({'option':'end'})
        print("\n Book not available..!!!!")

    @api.multi
    def cancle(self):
        self.write({'option':'cancle'})
        print("\n CANCLE...!!")

    # @api.multi
    # def wait(self):
    #     new = self.create({'name':'abc','description':'description','price':100})

    @api.model
    def create(self,vals):
        data = super(Book, self).create(vals)
        print("\n Create Method called..",self,vals,data)
        return data

    @api.multi
    def write(self,vals):
        data = super(Book,self).write(vals)
        print("\n Write Method called..",self,vals,data)
        return data

    @api.multi
    def unlink(self):
        print("\n unlink method called..")
        delete = super(Book,self).unlink()
        print(delete)

    @api.multi
    def copy(self,default=None):
        if default is None:
            default = {}
        default.update({'name':self.name + '**' + 'Duplicate'})
        return super(Book,self).copy(default)

    # @api.multi
    # def read(self,vals):
    #     print("\n Read method called..")
    #     read = super(Book,self).read(vals)
    #     print(read)
    #     return read