from odoo import models,fields

class Author(models.Model):
    _name = 'author.author'

    name = fields.Char('Name')
    email = fields.Text('Email')
    gender = fields.Selection([('m','Male'),('f','Female'),('o','Other')],'Gender')
    birthday = fields.Date('Birthday')
    about = fields.Html('About')

    book_id = fields.Many2one('book.book','Book')
