from odoo import models,fields,api
from datetime import datetime
import calendar
import time
import pytz

from odoo.exceptions import UserError

class User(models.Model):
    _name = 'user.user'

    name = fields.Char('First Name')
    lname = fields.Char('Last name')
    age = fields.Integer('Age')
    email = fields.Char('Email',required=True)
    about = fields.Text('About',readonly=True)

    audio_id = fields.Many2one('audio.audio', 'Audio')
    payment_id = fields.Many2one('payment.payment', 'Payment')

    # book_ids = fields.Many2many('book.book','book_user_rel','user_id','book_id',String='User')
    book_ids = fields.Many2many('book.book', 'user_book_rel', 'user_id', 'book_id', String='User')
    user_pay_ids = fields.One2many('payment.payment','user_id','User')

    event_id = fields.Many2one('event.event','Event')

    @api.multi
    def datetime(self):
        # new = self.create({'name':'abc','lname':'mno','age':20,'email':'abc@gmail.com','about':'NA'})
        localtime = time.localtime(time.time())
        print("\nLocal current time : ", localtime,"\n")
        local = time.asctime(time.localtime(time.time()))
        print("\n Local current time :", local)

        # datetime = datetime.now(tz=pytz.timezone('Indian/Maldives'))
        # print(datetime)
        # print(datetime.isoformat())  # isoformat international stand
        # print(datetime.strftime('%B %d ,%Y'))

        default = fields.Date.today()
        default1 = fields.Datetime.now()
        print("\n print today date : ",default,"\n")
        print(" print datetime : ", default1, "\n")

    @api.multi
    def end(self):
        print("\n End...!!!\n")

    @api.multi
    def default(self):
        print("\n Hello ...!!!!",fields.Datetime.now(),"\n")

    @api.multi
    def clickone(self):
        print("\n Keep your eyes open...",fields.Datetime.now(),"\n")
        raise UserError('Keep your eyes open..!!!')

    # @api.multi
    # def click(self):
    #     print("\n On click..!",self,self.user_pay_ids,self.name)
    #     return {
    #         'warning':{
    #             'title':'OnClick Action',
    #             'message':'Keep your eyes open..!!!'
    #         }
    #     }

    @api.onchange('audio_id')
    def change_person(self):
        print("\n\n OnChange......", self, self.audio_id)
        # 1) Set a Value
        self.payment_id = self.audio_id.payment_id.id
        # 2) Set a Domain
        # 3) Show warning message
        return {
            'warning': {
                'title': 'My Warning...',
                'message': 'Warning - OnChange..!!!'
            }
        }

